<?php
// HTTP
define('HTTP_SERVER', 'https://dokany.shop/admin/');
define('HTTP_CATALOG', 'https://dokany.shop/');

// HTTPS
define('HTTPS_SERVER', 'https://dokany.shop/admin/');
define('HTTPS_CATALOG', 'https://dokany.shop/');

// DIR
define('DIR_APPLICATION', '/home/o9c6wd85vx7o/public_html/admin/');
define('DIR_SYSTEM', '/home/o9c6wd85vx7o/public_html/system/');
define('DIR_IMAGE', '/home/o9c6wd85vx7o/public_html/image/');
define('DIR_STORAGE', '/home/o9c6wd85vx7o/storage/');
define('DIR_CATALOG', '/home/o9c6wd85vx7o/public_html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'dokany');
define('DB_PASSWORD', ')tNWup2][043');
define('DB_DATABASE', 'dokanyshop');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
